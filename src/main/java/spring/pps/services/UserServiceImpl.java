package spring.pps.services;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import spring.pps.data.entities.RoleEnum;
import spring.pps.data.repositories.RoleRepository;
import spring.pps.data.repositories.UserRepository;
import spring.pps.data.entities.Role;
import spring.pps.data.entities.User;

import java.util.Objects;
import java.util.Set;

@Slf4j
@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final PasswordEncoder bCryptPasswordEncoder;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, RoleRepository roleRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.bCryptPasswordEncoder = passwordEncoder;
    }


    @Transactional(readOnly = true)
    @Override
    public UserDetails loadUserByUsername(String username) {
        log.info("find by username "+username);

        return userRepository.findDistinctByUsername(username);
    }

    public void save(User user) {
        //todo: check authorities
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        Role role= roleRepository.findDistinctByName(RoleEnum.ROLE_USER.name());
        if (role!=null)
            user.setRoles(Set.of(role));

        userRepository.save(user);
    }


    @Transactional
    public void saveToDefaults(User user) {
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));

        Role role= roleRepository.findDistinctByName(RoleEnum.ROLE_USER.name());
        if (role!=null)
            user.setRoles(Set.of(role));


        user.setEnabled(true);
        user.setAccountNonLocked(true);
        user.setAccountNonExpired(true);
        user.setCredentialsNonExpired(true);
        userRepository.save(user);
    }
}
