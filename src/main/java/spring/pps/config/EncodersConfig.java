package spring.pps.config;


import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.DelegatingPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.crypto.password.Pbkdf2PasswordEncoder;

import java.util.HashMap;
import java.util.Map;

@Slf4j
@Configuration
public class EncodersConfig {

    @Bean
    public PasswordEncoder passwordEncoder() {
        log.warn("passwordEncoder()");
        String currentId = "pbkdf2.2018";
        String secret = "secret";
        Map<String, PasswordEncoder> encoders = new HashMap<>();
        encoders.put("bcrypt", new BCryptPasswordEncoder());
        encoders.put(currentId, new Pbkdf2PasswordEncoder(secret,12, 181));
        return new DelegatingPasswordEncoder(currentId, encoders);
    }
}
