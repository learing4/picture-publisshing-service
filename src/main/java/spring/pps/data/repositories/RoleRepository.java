package spring.pps.data.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import spring.pps.data.entities.Role;


public interface RoleRepository extends JpaRepository<Role, Long> {
    Role findDistinctByName(String name);
}
